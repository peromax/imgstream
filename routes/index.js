
/*
 * GET home page.
 */

exports.index = function(req, res, models){
  models.posts.getPage(1,function (err,docs) {
      console.log(docs);
      res.render('index', 
      { 
        title: 'Проба пера. Дроп файлов',
        posts: docs
      });
    });
  
};

exports.shot = function(req,res, models){
  console.log(req.params.id);
  models.posts.findOne({_id: req.params.id}, function(err, doc){
    console.log(doc);
    console.log(err);
    res.render('shot',
    {
      title: 'Шот Ивана Личина',
      post : doc
    });
  });
}

exports.game = function(req, res){
  res.render('game', { title: 'Чудо веб сокеты' })
}

exports.upload = function(req, res){
  var fs = require('fs');
  fs.exists = fs.exists || require('path').exists;
  var path = __dirname + '/../public/images/upload/';
  var pathNewFile = path + req.body.filename;

  fs.stat(pathNewFile, function (error, stats) {
    if (!error) {
    
    // Adding timestamp for exist filename 
    if (stats.isFile()){
      newFileName = req.body.filename.substring(0,req.body.filename.lastIndexOf("."));
      var ext = req.body.filename.substring(req.body.filename.lastIndexOf("."));
      var time = new Date().getTime();
      
      newFileName += time;
      pathNewFile = path + newFileName + ext;
      };
    };

    fs.writeFile( pathNewFile, req.body.data, 'binary', function(err){
      if(err){
        console.log(err);
      }else{
        console.log('ok');
      }
    });
  });

  
}