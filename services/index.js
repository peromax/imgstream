
// save file on fylesystem
exports.uploadFile = function(filename, binaryData , callback, im){
  var fs = require('fs');
  var path = require('path');
  var imgPath = __dirname + '/../public/images/upload/';
  var pathNewFile = imgPath + filename;

  var generateSrcForImgMed = function(src){
  	var medPath = imgPath + 'medium/';
  	var basename = path.basename(src);

  	return medPath + basename;
  };


  fs.stat(pathNewFile, function (error, stats) {
    if (!error) {
    
    // Adding timestamp for exist filename 
    if (stats.isFile()){
      newFileName = filename.substring(0,filename.lastIndexOf("."));
      var ext = filename.substring(filename.lastIndexOf("."));
      var time = new Date().getTime();
      
      newFileName += time;
      filename = newFileName + ext;
      pathNewFile = imgPath + newFileName + ext;
      };
    };

    fs.writeFile( pathNewFile, binaryData, 'binary', function(err){
      if(err){
        console.log(err);
      }else{
        // Если файл записался
        im.identify(pathNewFile, function(err, features){

          if(features.width > 800){
            // делаем его уменьшенную копию
            im.resize({
              srcPath: pathNewFile,
              dstPath: generateSrcForImgMed(pathNewFile),
              width:   800
            }, function(err, stdout, stderr){
            if (err) throw err

              var data = {
                file : filename,
                img : '/images/upload/' + filename,
                imgMed : '/images/upload/medium/' + filename,
                src : pathNewFile
                }
                callback(data);
              console.log('resized '+ pathNewFile +' to fit within width:800px')
            });
          } else {
            var data = {
              file : filename,
              img : '/images/upload/' + filename,
              imgMed : '/images/upload/' + filename,
              src : pathNewFile
            }
            callback(data);
          }
        });	
      }
    });
  });
}