
/**
 * Module dependencies.
 */

var express = require('express'),
  util = require('util'),
  mongoose = require('mongoose'),
  im = require('imagemagick'),
  moment = require('moment'),
  path = require('path'),
  format = require('util').format;

// moment.js language
moment.lang('ru');

// mongoose connect
mongoose.connect('mongodb://localhost/imflow');
var post = require('./models/imgPost');

var models = {
  posts : post
};


// end mongoose

var app = module.exports = express.createServer();
var io = require('socket.io').listen(app);
var services = require('./services');
// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser({uploadDir: __dirname + "/public/uploads"}));
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes
routes = require('./routes');

app.get('/', function(req,res){routes.index(req,res,models)});
app.get('/shot/:id', function(req,res){routes.shot(req,res,models)});
app.get('/game', routes.game);
app.post('/upload', routes.upload);



app.listen(3333, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});




io.sockets.on('connection', function(socket){

  socket.on('turn', function(data){
    socket.broadcast.emit('turned', data)
  });


  // Upload via Socket.io
  
  /**
  *  data = {
  *    file: 'sample.png',
  *    bin : 'sad324ewae2weaaEVE3acrsd'
  *  }
  */
  socket.on('transferFile', function(data){
    console.log(data.file);
    services.uploadFile(data.file, data.bin, function(data){

      var newImgPost = new models.posts;
      newImgPost.img = data.img;
      newImgPost.imgMed = data.imgMed;
      newImgPost.save(function(err){
        if(err)
          throw new Error(err);
        else{
          socket.emit('showPic', newImgPost);
          socket.broadcast.emit('showPic', newImgPost);    
        }
      });
      
    }, im);
  });

});
