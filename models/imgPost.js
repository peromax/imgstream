/**
* Post Mongoose Model
*/

const mongoose = require('mongoose')
    , moment = require('moment')
    , ObjectId = mongoose.Schema.ObjectId;

var ImgPost = new mongoose.Schema({
  img : { type:String, default: '/images/empty.png'},
  imgMed : { type : String, default : '/images/empty.png'},
  date : { type:Date, default: Date.now }
});

ImgPost.methods.getTime = function getTime(){
  var mDate = moment(this.date);
  return mDate.format('D MMMM YYYY, hh:mm')
}

// return 15 posts from i count
ImgPost.statics.getBulkPosts = function(i, callback){
  return this.find().skip(i).desc('_id').limit(10).find({},callback);
};
// return posts on page
ImgPost.statics.getPage = function(page, callback){
  var page = page || 1;
  var limit = 10;

  var skip = limit * page - limit;

  return this.find().skip(skip).sort('-_id').limit(limit).find({},callback);
};



module.exports = mongoose.model('Post',ImgPost);