;(function($,doc,win) {

  "use strict";


  function DropFileWidget(el, opts){
    this.el = $(el);
    
    // defaults options
    this.defaults = {
      filetypes: ['png','jpg'],
      maxsize : 8 * 1000 * 10,
      url_upload : null,
      onload : function(filename, binary){
        alert('Файл "'+ filename +'" загружен!');
      },
      ondrag : function(e){
        console.log('Я чувствую: надо мной интересный контент');
      },
      ondragout : function(e){
        console.log('Убрали, можно расслабится');
      },
      ondrop : function(e){
        console.log('Мы получили от вас файл =)');
      },
      onprogress : function(e){
        console.log('Всего данных: ' + e.total);
        console.log('Загружено: ' + e.loaded);
      }
    };
    
    this.opts = $.extend(this.defaults, opts);

    this.init();
  }

  DropFileWidget.prototype = {
    init : function(){
      var self = this;
      this.el.bind('dragenter', function(e){self.onDragEnter.apply(self,[e])} )
        .bind('dragleave', function(e){self.onDragLeave.apply(self,[e])} )
        .bind('dragover', function(e){self.onDragEnter.apply(self,[e])} )
        .bind('drop', function(e){self.onDrop.apply(self,[e])});
    },
    ignoreDrag : function (e) {
      e.originalEvent.stopPropagation();
      e.originalEvent.preventDefault();
    },

    onDragEnter : function (e){
      this.ignoreDrag(e);
      this.el.addClass('drag-enter');
      //callback
      this.opts.ondrag(e);
    },

    onDragLeave : function(e){
      this.ignoreDrag(e);
      this.el.removeClass('drag-enter');
      //callback
      this.opts.ondragout(e);
    },

    onDrop : function (e){
      this.ignoreDrag(e);
      var dt = e.originalEvent.dataTransfer;
      var files = dt.files;

      if(dt.files.length > 0){
        var file = dt.files[0];
        this.readFile(file);
      }

      this.opts.ondrop(e);
    },

    readFile : function(file){
      var self = this;
      var reader = new FileReader();

      reader.onload = (function(f){
        return function(e){
          // callback
          self.opts.onload(f.name, e.target.result);
          // send to Server
          self.sendData(f.name,e.target.result);
        };
      })(file);

      reader.onprogress = function(evt){
        self.opts.onprogress(evt);
        if (evt.lengthComputable) {
          var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
          // Increase the progress bar.
          if (percentLoaded < 100) {
            // progress.addClass('show');
            // progress.style.width = percentLoaded + '%';
            // progress.text(percentLoaded + '%');
          };
        };
      };
    
      // Начинаем читать файл
      reader.readAsBinaryString(file);
    },

    sendData : function (filename, data){
      var self = this;
      if(self.opts.url_upload)
        $.post(self.opts.url_upload, { filename : filename, data : data });
    }

  };
  
  $.fn.dropfile = function (opts){
    return this.each(function(){
      new DropFileWidget(this, opts);
    });
  };

})(jQuery, document, window);